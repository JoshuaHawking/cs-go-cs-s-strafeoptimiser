#include <sourcemod>
#include <sdktools>

public Plugin myinfo = 
{
	name = "CS:GO/CSS Strafe Optimiser", 
	author = "Josh", 
	description = "Provides an Strafe Optimiser.", 
	version = "1.0.0", 
	url = "https://bitbucket.org/JoshuaHawking/cs-go-cs-s-strafeoptimiser"
};

// Globals
bool g_bStrafeEnabled[MAXPLAYERS + 1];

float g_flSidespeed = 400.0;

public void OnPluginStart()
{
	RegAdminCmd("sm_strafe", Cmd_ToggleStrafe, ADMFLAG_CHEATS, "Toggles strafe optimiser.")
	RegAdminCmd("sm_strafer", Cmd_ToggleStrafe, ADMFLAG_CHEATS, "Toggles strafe optimiser.")
	RegAdminCmd("sm_optimiser", Cmd_ToggleStrafe, ADMFLAG_CHEATS, "Toggles strafe optimiser.")
	RegAdminCmd("sm_strafeoptimiser", Cmd_ToggleStrafe, ADMFLAG_CHEATS, "Toggles strafe optimiser.")
	
	// Check whether it's CSS or CS:GO
	if(GetEngineVersion() == Engine_CSGO)
	{
		g_flSidespeed = 450.0;
	}
	else if(GetEngineVersion() == Engine_CSS)
	{
		g_flSidespeed = 400.0;
	}
	else
	{
		LogMessage("Warning: Unsupported game for autostrafer.")
	}
}

public OnClientConnected(int client)
{
	// Disable on connecting
	g_bStrafeEnabled[client] = false;
}

// Toggle command
public Action Cmd_ToggleStrafe(int client, int args)
{
	if (client <= 0)
	{
		PrintToChat(client, "Cannot call this command from the console!")
		return Plugin_Handled;
	}
	
	// Toggle
	g_bStrafeEnabled[client] = !g_bStrafeEnabled[client]
	
	// Alert user
	char szMessage[256];
	FormatEx(szMessage, sizeof(szMessage), "[SM] Strafe Optimiser: %s", (g_bStrafeEnabled[client] ? "ON" : "OFF"))
	PrintToChat(client, szMessage)
	
	// Log toggle
	LogAction(client, -1, "%N toggled their strafe optimiser (%s)", client, (g_bStrafeEnabled[client] ? "ON" : "OFF"))
	return Plugin_Handled;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (!IsClientInGame(client) || !IsPlayerAlive(client))
		return Plugin_Continue;
	
	if (IsFakeClient(client))
		return Plugin_Continue;
	
	// If they are currently on the ground, or on a ladder, disable the autostrafe
	if (GetEntityFlags(client) & FL_ONGROUND || GetEntityMoveType(client) & MOVETYPE_LADDER)
		return Plugin_Continue;

	// If they are currently pushing buttons, disable the autostrafe
	if (buttons & IN_MOVELEFT || buttons & IN_MOVERIGHT || buttons & IN_FORWARD || buttons & IN_BACK)
		return Plugin_Continue;

	if (g_bStrafeEnabled[client])
	{
		if(mouse[0] > 0)
		{
			vel[1] = g_flSidespeed;
			buttons |= IN_MOVERIGHT;
		}
 
		else if(mouse[0] < 0)
		{
			vel[1] = -g_flSidespeed;
			buttons |= IN_MOVELEFT;
		}
		return Plugin_Changed;
	}
	
	return Plugin_Continue;
}