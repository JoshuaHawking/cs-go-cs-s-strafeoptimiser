# CS:GO/CS:S StrafeOptimiser
A CS:GO/CS:S StrafeOptimiser plugin for Sourcemods. Admins with the CHEAT flag can use !sm_strafer to enable an strafe optimiser. This will cause them to automatically press A/D whenever they strafe left/right respectively.

I'm not responsible for any anti-cheat this sets off, any bans you end up giving yourself or anything of the kind. Please note that this uses logic from cheating software and will therefore be detected by most SM anti-cheats. Use at your own caution.

# How to use
Drop the `sm_strafeoptimise.smx` into the `addons/sourcemod/plugins` folder and reload the map. From there, any admin with the cheats flag will be able to do `!strafer` to toggle the strafe optimiser.

# Any improvements?
Feel free to submit issues/pull requests.